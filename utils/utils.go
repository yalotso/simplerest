package utils

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
)

func CreateToken(alg, secretKey string, userId int, exp time.Duration) string {
	method := jwt.GetSigningMethod(alg)
	token := jwt.NewWithClaims(method, jwt.MapClaims{
		"user_id": userId,
		"exp":     time.Now().Add(exp).Unix(),
	})
	tokenString, _ := token.SignedString([]byte(secretKey))
	return tokenString
}

func ParseToken(secretKey, tokenString string) (int, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secretKey), nil
	})
	if err != nil {
		return 0, err
	}
	userId := token.Claims.(jwt.MapClaims)["user_id"].(float64)
	return int(userId), nil
}
