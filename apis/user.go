package apis

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"simplerest/app"
	"simplerest/models"
	"simplerest/utils"
	"strconv"
	"time"
)

type userService interface {
	New(rs app.RequestScope, user *models.User) error
	Login(rs app.RequestScope, username, password string) (*models.User, error)
	GetAll(rs app.RequestScope) ([]models.User, error)
	Delete(rs app.RequestScope, id int) (*models.User, error)
}

type userResource struct {
	service userService
}

func ServeUserResource(rg *gin.RouterGroup, service userService) {
	r := &userResource{service}
	rg.POST("/users/login", r.login)

	rg.Use(JWT)
	rg.POST("/users", r.new)
	rg.GET("/users", r.getAll)
	rg.DELETE("/users/:id", r.delete)
}

func (r *userResource) login(c *gin.Context) {
	var req struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	dec := json.NewDecoder(c.Request.Body)
	err := dec.Decode(&req)
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "BAD_REQUEST", "message": "check the request body"})
		return
	}
	rs := app.GetRequestScope(c)
	user, err := r.service.Login(rs, req.Username, req.Password)
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "BAD_REQUEST", "message": "check the request body"})
		return
	}
	token := utils.CreateToken(app.Config.JWTSigningMethod, app.Config.JWTSigningKey, user.Id, time.Hour*72)
	c.JSON(http.StatusOK, gin.H{"token": token})
}

func (r *userResource) new(c *gin.Context) {
	var newUser models.User
	dec := json.NewDecoder(c.Request.Body)
	err := dec.Decode(&newUser)
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "BAD_REQUEST", "message": "check the request body"})
		return
	}

	rs := app.GetRequestScope(c)
	err = r.service.New(rs, &newUser)
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "BAD_REQUEST", "message": "check the request body"})
		return
	}

	c.JSON(http.StatusOK, &newUser)
}

func (r *userResource) getAll(c *gin.Context) {
	users, err := r.service.GetAll(app.GetRequestScope(c))
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "BAD_REQUEST", "message": "user not found"})
		return
	}
	c.JSON(http.StatusOK, users)
}

func (r *userResource) delete(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "BAD_REQUEST", "message": "id must be a number"})
		return
	}
	user, err := r.service.Delete(app.GetRequestScope(c), id)
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"code": "BAD_REQUEST", "message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, user)
}
