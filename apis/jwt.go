package apis

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"simplerest/app"
	"simplerest/utils"
)

func JWT(c *gin.Context) {
	token := c.Request.Header.Get("Authorization")
	if token == "" {
		c.Abort()
		c.JSON(http.StatusUnauthorized, gin.H{"code": "UNAUTHORIZED", "message": "token is not valid"})
		return
	}
	userId, err := utils.ParseToken(app.Config.JWTVerificationKey, token)
	if err != nil {
		c.Abort()
		c.Error(err)
		c.JSON(http.StatusUnauthorized, gin.H{"code": "UNAUTHORIZED", "message": "token is not valid"})
		return
	}
	app.GetRequestScope(c).SetUser(userId)
}
