package daos

import (
	"simplerest/app"
	"simplerest/models"
)

type UserDAO struct{}

func NewUserDAO() *UserDAO {
	return &UserDAO{}
}

func (dao *UserDAO) GetByUsername(rs app.RequestScope, username string) (*models.User, error) {
	var user models.User
	err := rs.Tx().First(&user, "username=?", username).Error
	return &user, err
}

func (dao *UserDAO) New(rs app.RequestScope, user *models.User) error {
	user.Id = 0
	err := rs.Tx().Create(&user).Error
	return err
}

func (dao *UserDAO) Delete(rs app.RequestScope, id int) (*models.User, error) {
	var user models.User
	err := rs.Tx().First(&user, id).Delete(&user, id).Error
	return &user, err
}

func (dao *UserDAO) GetAll(rs app.RequestScope) ([]models.User, error) {
	var users []models.User
	err := rs.Tx().Find(&users).Error
	return users, err
}

func (dao *UserDAO) GetUsers(rs app.RequestScope) ([]models.User, error) {
	var users []models.User
	err := rs.Tx().Find(&users, "role=?", models.RoleUser).Error
	return users, err
}
