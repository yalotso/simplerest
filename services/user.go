package services

import (
	"errors"
	"simplerest/app"
	"simplerest/models"
)

type userDAO interface {
	GetByUsername(rs app.RequestScope, username string) (*models.User, error)
	New(rs app.RequestScope, user *models.User) error
	GetAll(rs app.RequestScope) ([]models.User, error)
	GetUsers(rs app.RequestScope) ([]models.User, error)
	Delete(rs app.RequestScope, id int) (*models.User, error)
}

type UserService struct {
	dao userDAO
}

func NewUserService(dao userDAO) *UserService {
	return &UserService{dao}
}

func (s *UserService) Login(rs app.RequestScope, email, password string) (*models.User, error) {
	user, err := s.dao.GetByUsername(rs, email)
	if err != nil {
		return nil, err
	}
	if password != user.Password {
		return nil, errors.New("wrong password")
	}
	return user, nil
}

func (s *UserService) New(rs app.RequestScope, user *models.User) error {
	err := user.Validate()
	if err != nil {
		return err
	}
	err = s.dao.New(rs, user)
	if err != nil {
		return err
	}
	return nil
}

func (s *UserService) GetAll(rs app.RequestScope) ([]models.User, error) {
	var users []models.User
	var err error
	user := rs.User()
	if user.Role == models.RoleUser {
		users, err = s.dao.GetUsers(rs)
	} else {
		users, err = s.dao.GetAll(rs)
	}
	return users, err
}

func (s *UserService) Delete(rs app.RequestScope, id int) (*models.User, error) {
	user := rs.User()
	if user.Role != models.RoleAdmin {
		return nil, errors.New("access denied")
	}
	user, err := s.dao.Delete(rs, id)
	return user, err
}
