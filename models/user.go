package models

import (
	"errors"
)

const RoleAdmin = "admin"
const RoleUser = "user"

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

func (user *User) Validate() error {
	if user.Username == "" {
		return errors.New("username required")
	}
	if user.Password == "" {
		return errors.New("password required")
	}
	if user.Role == "" {
		return errors.New("role required")
	}
	if user.Role != RoleUser && user.Role != RoleAdmin {
		return errors.New("role does not exist")
	}
	return nil
}
