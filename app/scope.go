package app

import (
	"github.com/jinzhu/gorm"
	"simplerest/models"
	"time"
)

type RequestScope interface {
	Logger
	User() *models.User
	SetUser(id int)
	Tx() *gorm.DB
	SetTx(tx *gorm.DB)
	Rollback() bool
	SetRollback(bool)
	Now() time.Time
}

type requestScope struct {
	Logger
	now      time.Time
	user     *models.User
	rollback bool
	tx       *gorm.DB
}

func (rs *requestScope) User() *models.User {
	return rs.user
}

func (rs *requestScope) SetUser(id int) {
	var user models.User
	err := rs.Tx().First(&user, id).Error
	if err != nil {
		rs.Errorf("user %d not found: %s", id, err)
	}
	rs.user = &user
}

func (rs *requestScope) Tx() *gorm.DB {
	return rs.tx
}

func (rs *requestScope) SetTx(tx *gorm.DB) {
	rs.tx = tx
}

func (rs *requestScope) Rollback() bool {
	return rs.rollback
}

func (rs *requestScope) SetRollback(v bool) {
	rs.rollback = v
}

func (rs *requestScope) Now() time.Time {
	return rs.now
}

func newRequestScope(l Logger, now time.Time) RequestScope {
	return &requestScope{
		Logger: l,
		now:    now,
	}
}
