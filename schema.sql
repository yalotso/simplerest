create table users
(
	id int auto_increment primary key,
	username varchar(55) not null,
	password varchar(10) not null,
	role varchar(55) not null
);